require 'csv'
class CsvController < ApplicationController

	def read
		files = ["buildings.csv", "people.csv"]
		files.each do |file|
			csv = CSV.read(file, :headers => true)
			csv.each do |row|
				hash = row.to_hash
				klass = if file == "buildings.csv"
					Building
				elsif file == "people.csv"
					Person
				end
				if klass.where(:reference => row["reference"]).exists?
					need_update?(klass, hash)
				else
					klass.create row.to_hash
				end
			end
 		end
	end

	private
	def need_update?(klass, data)
		row = klass.where(:reference => data["reference"]).first
		klass::VERSIONED_ATTRIBUTES.each do |key|
			if data[key] == row.send(key)
				data.delete(key)
			end
		end
		row.update(data)
	end
end
