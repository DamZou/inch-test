require 'test_helper'

class BuildingTest < ActiveSupport::TestCase
  
	def create_building
		Building.new(	 :reference => 'toto',
									 :address => '10 Rue La bruyère', 
	   						   :zip_code => "75009", 
	   						   :city => "Paris", 
	   						   :country => "France", 
	   						   :manager_name => "Martin Faure")
	end

  def test_presence_reference
	   building = create_building_without :reference
	   building.reference = nil
	   assert !building.save, "reference can't be blank"
	end

	def test_presence_address
	   building = create_building
	   building.address = nil
	   assert !building.save, "address can't be blank"
	end

	def test_presence_zip_code
	   building = create_building
	   building.zip_code = nil
	   assert !building.save, "zip_code can't be blank"
	end

	def test_presence_city
	   building = create_building
	   building.city = nil
	   assert !building.save, "city can't be blank"
	end

	def test_presence_country
	   building = create_building
	   building.country = nil
	   assert !building.save, "country can't be blank"
	end

	def test_presence_manager_name
	   building = create_building
	   building.manager_name = nil
	   assert !building.save, "manager_name can't be blank"
	end

end
