require 'test_helper'

class PersonTest < ActiveSupport::TestCase

	def create_person
		Person.new(:reference => "toto", :firstname => "Henri", 
				            :lastname => "Dupont", 
						    :home_phone_number => "0123456789",
					  	    :mobile_phone_number => "0623456789",
							:email => "h.dupont@gmail.com",
							:address => "10 Rue La bruyère")
	end

	def setup
		@person = create_person
	end

	def test_presence_reference
		@person.reference = nil
	  assert !@person.save, "reference can't be blank"
	end

	def test_presence_firstname
		@person.firstname = nil
	  assert !person.save, "firstname can't be blank"
	end

	def test_presence_lastname
		@person.lastname = nil
	  assert !person.save, "lastname can't be blank"
	end

	def test_presence_home_phone_number
		@person.home_phone_number = nil
	  assert !person.save, "home phone number can't be blank"
	end

	def test_presence_mobile_phone_number
		@person.mobile_phone_number = nil
	  assert !person.save, "mobile phone number can't be blank"
	end

	def test_presence_email
		@person.email = nil
	  assert !person.save, "email can't be blank"
	end

	def test_presence_address 
		@person.address
	  assert !person.save, "address  can't be blank"
	end

end
